import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.event.ActionListener;

public class Window {
	public JLabel tituloHeader = new JLabel("<html><body>Bienvenido al Juego<br>Empieza el jugador O</body></html>");
	public JTextPane tablero;
	public JFrame frame;	
	/*
	 * Variables
	 * Mesa mesa: Llamos a la clase mesa 
	 * sel: seleccion del usuario boton 1,2,3,4,5,6
	 * finJuego: boolean que inicialmente esta a false, y pasa a true si el juego llega a 25 jugadas
	 * tableroDesign: tableroDesigning que devuelve el diseño del tablero
	 * contadorTurnos: las veces que van jugando los usuarios, aumenta en cada click
	 * player: caracter que mete el usuario en el tablero 
	 */
	private Mesa mesa = new Mesa();
	private int sel = 0;
	private boolean finJuego = false;
	private String tableroDesign = "";
	private int contadorTurnos = 0;
	private char player = 'O';
	/*
	 * Creamos la ventana.
	 */
	public Window() {
		initialize();
	}
	/* Turno Comprueba que jugador le toca para cada turno con un switch
	 * contadorTurnos Variable que va aumentando segun el usuario pulsa
	 */
	public void turno(){
		contadorTurnos++;
		switch(player){
			case 'O':
				tituloHeader.setText("Turno del jugador X "+contadorTurnos);
				player = 'X';
				break;
			case 'X':
				tituloHeader.setText("Turno del jugador O "+contadorTurnos);
				player = 'O';
				break;
		}
		maximoTurno();
	}
	/*
	 * Comprobamos que no se pueda jugar mas de 25 veces
	 */
	private void maximoTurno() {
		if(contadorTurnos>=25){
			tituloHeader.setText("Fin del juego");
			finJuego = true;
		}
	}
	/*
	 * Inicializamos el contenido de la ventana y sus respectivos botones
	 */
	private void initialize() {
		tableroDesign = mesa.tablero();
		frame = new JFrame();
		frame.setBounds(100, 100, 633, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		tablero = new JTextPane();
		tablero.setText(tableroDesign);
		tablero.setBounds(20, 48, 417, 320);
		tablero.setFont(new Font("Arial",Font.PLAIN,20));
		frame.getContentPane().add(tablero);
		
		JButton btnNewButton = new JButton("1");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego){
					sel=1;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();
				}
			}
		});
		btnNewButton.setBounds(15, 23, 39, 22);
		frame.getContentPane().add(btnNewButton);
				
		JButton button_1 = new JButton("2");
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego){
					sel=2;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();	
				}
			}
		});
		button_1.setBounds(85, 23, 39, 22);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("3");
		button_2.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego) {
					sel=3;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();
				}

			}
		});
		button_2.setBounds(158, 23, 39, 22);
		frame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("4");
		button_3.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego) {
					sel=4;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();
				}
			}
		});
		button_3.setBounds(230, 23, 39, 22);
		frame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("5");
		button_4.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego) {
					sel=5;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();
				}
			}
		});
		button_4.setBounds(303, 23, 39, 22);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("6");
		button_5.setFont(new Font("Tahoma", Font.PLAIN, 8));
		button_5.setBounds(374, 23, 39, 22);
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!finJuego) {
					sel=6;
					mesa.meteFicha(sel,player);
					tableroDesign = mesa.tablero();
					tablero.setText(tableroDesign);
					turno();
				}
			}
		});
		frame.getContentPane().add(button_5);
		tituloHeader.setBounds(464, 40, 153, 100);
		frame.getContentPane().add(tituloHeader);
	}
}