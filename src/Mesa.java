public class Mesa {
	private String [][] total = new String [6][6];
	private int sel = 0;
	private int numVacios = 0;

	/*
	 * Constructor
	 */
	public Mesa() {
		int o=0;
		for (int i=0;i<total.length;i++){
			for(int z=0;z<total[0].length;z++){
				total [i][z] = "[    ]\t";
				o=z;
			}
			total[i][o]="[    ]\n";
		}
	}
	/*
	 * Comprobamos si hay valores nulls
	 */
	public void cuentaVacios() {
		numVacios=0;
		for (int i = 0;i<total[0].length;i++){
			if(total[i][sel].indexOf(" ")>-1){
				numVacios++;
			}
		}
		numVacios-=1;
	}
	/*
	 * Creamos el tablero
	 */
	public String tablero () {
		String str = "";
		for (int i=0;i<total.length;i++){
			for(int z=0;z<total[0].length;z++){
				str += total [i][z];
			}
			str += "\n";
		}
		return str;
	}
	/*
	 * Metemos la ficha en la fila que seleccione el usuario
	 */
	public void meteFicha(int sel,char player){	
			this.sel=sel-1;
			cuentaVacios();
			if(numVacios>=0){
				total[numVacios][sel-1]="["+player+"]";
				if(sel==total[numVacios].length){				
					total[numVacios][sel-1]+="\n";				
				}else{
					total[numVacios][sel-1]+="\t";
				}
			}
	}
	/*
	 * Comprueba el ganador
	 * @return Ganador jugador 1 o 2
	 */
	public void compruebaGanador (){
		compruebaHorizontal();
		compruebaDiagonal();
		compruebaDiagonalIzquierda();
		compruebaDiagonalDerecha();
	}
	public void compruebaHorizontal() {
		
	}
	public void compruebaDiagonal() {
		
	}
	public void compruebaDiagonalIzquierda() {
		
	}
	public void compruebaDiagonalDerecha() {
		
	}
}
